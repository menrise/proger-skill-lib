# Proger Skill Lib
##Common
https://refactoring.guru/ru

https://designpatternsphp.readthedocs.io/ru/latest/README.html

#PHP
php the right way

https://phptherightway.com/ - en

http://getjump.github.io/ru-php-the-right-way/ - ru

##JS
https://learn.javascript.ru/

http://jstherightway.org/

http://bonsaiden.github.io/JavaScript-Garden/ru/

##Boilerplates
https://github.com/gothinkster/realworld

##Interactive cources
https://egghead.io/

https://thinkster.io/

##CheatsSheets
http://overapi.com/
https://devhints.io

##Languages APIs
http://overapi.com/javascript

